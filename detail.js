api()
async function api(){
  const code = window.localStorage.getItem('countrycode');
  console.log(code);
  const response = await fetch('https://restcountries.eu/rest/v2/alpha/'+`${code}`+'');
	const data = await response.json();
	console.log(data);
	console.log("We are done!");

  	document.getElementById('india').innerHTML+=
        '<!-- India name -->\n'+
        '<div class="row justify-content-center">\n'+
        '<div class="col-md-6 col-sm-12">\n'+
        '<h1 class=""> '+`${data.name}`+' </h1>\n'+
        '</div>\n'+
      	'</div>\n'+
      	'<!-- india image and detail -->\n'+
        '<div class="row justify-content-center">\n'+
        '<div class="col-md-6 col-sm-12 pb-4 ">\n'+
        '<div class="row">\n'+
        '<div class="col-md-6 col-sm-12 ">\n'+
        '<!-- image -->\n'+
        '<img src="'+`${data.flag}`+'" class="img-fluid ">\n'+
        '</div>\n'+
        '<div class="col-md-6 col-sm-12 ">\n'+ 
        '<!-- c-detail-->\n'+
        '<div class="row justify-content-start">\n'+
        '<div class="col-12">\n'+
        '<span class="text-left font-india">Native Name: '+`${data.nativeName}`+'</span>\n'+
        '</div>\n'+
        '</div> \n'+
        '<div class="row justify-content-start">\n'+
        '<div class="col-12">\n'+
        '<span class="text-left font-india">Capital: '+`${data.capital}`+'</span>\n'+
        '</div>\n'+
        '</div>\n'+
        '<div class="row justify-content-start">\n'+
        '<div class="col-12">\n'+
        '<span class="text-left font-india">Population: '+`${data.population}`+'</span>\n'+
        '</div>\n'+
        '</div>\n'+
        '<div class="row justify-content-start">\n'+
        '<div class="col-12">\n'+
        '<span class="text-left font-india">Region:'+`${data.region}`+'</span>\n'+
        '</div>\n'+
        '</div>\n'+
        '<div class="row justify-content-start">\n'+
        '<div class="col-12">\n'+
        '<span class="text-left font-india">Sub-region:'+`${data.subregion}`+'<span>\n'+
        '</div>\n'+
        '</div>\n'+
        '<div class="row justify-content-start">\n'+
        '<div class="col-12">\n'+
        '<span class="text-left font-india">Area:'+`${data.area}`+' Km2</span>\n'+
        '</div>\n'+
        '</div>\n'+
        '<div class="row justify-content-start">\n'+
        '<div class="col-12">\n'+
        '<span class="text-left font-india">Country Code: '+`${data.callingCodes}`+'</span>\n'+
        '</div>\n'+
        '</div>\n'+
        '<div class="row justify-content-start">\n'+
        '<div class="col-12">\n'+
        '<span class="text-left font-india">Languages: '+`${data.languages[0].name}`+'  </span>\n'+
        '</div>\n'+
        '</div>\n'+
        '<div class="row justify-content-start">\n'+
        '<div class="col-12">\n'+
        '<span class="text-left font-india">Currencies: '+`${data.currencies[0].name}`+'</span>\n'+
        '</div>\n'+
        '</div>\n'+
        '<div class="row justify-content-start">\n'+
        '<div class="col-12">\n'+
        '<span class="text-left font-india">Timezones: '+`${data.timezones}`+'</span>\n'+
        '</div>\n'+
        '</div>\n'+
        '</div>\n'+
        '</div>\n'+
        '</div>\n'+
        '</div>\n'
        
        for (let i in data.borders){
          document.getElementById('flag').innerHTML+=

          '<img src="https://restcountries.eu/data/'+`${data.borders[i]}`.toLowerCase()+'.svg" class="img-fluid  col-md-3 col-sm-12  p-2">\n'
        }

  }
