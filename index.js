
let countries;
const searchBar = document.getElementById('searchBar');

async function api(){
	const response = await fetch('https://restcountries.eu/rest/v2/all');
	countries = await response.json();
	console.log("We are done!");
	console.log(countries);
	showCountry(countries);

}

searchBar.addEventListener('keyup', (e) => {
    const searchString = e.target.value.toLowerCase();
	const filteredCharacters  = countries.filter((country)=> {
		return(country.name.toLowerCase().includes(searchString));
	});
	console.log(filteredCharacters);
	showCountry(filteredCharacters);
	
});


function showCountry(countries)
{
		let card =document.getElementById('countries');
		let str="";
		for (let i of countries){
			let time = dateTime(i.timezones[0]);
		    str +=
			`<div class="row justify-content-center ">
			 	<div class="col-md-6 col-sm-6 border mb-3 p-1">
			 		<div class="row justify-content-center ">
			 			<div class="col-md-4 col-sm-12 ">
			 				<!-- image -->
			 				<img src="${i.flag}" class="img-fluid ">
						</div>
			 			<div class="col-md-8 col-sm-12">
			 				<!-- c-name -->
			 				<div class="row justify-content-start">
			 					<div class="col-md-6 col-sm-4">
			 						<h4 class="text-left font-weight-bold">${i.name}</h4>
			 					</div>
			 				</div>
			 				<!-- currency -->
			 				<div class="row ">
			 					<div class="col-12">
			 						<span class="spamfont" >Currency: ${i.currencies[0].name}</span>
			 					</div>
			 				</div>
			 				<!-- date and time -->
			 				<div class="row ">
			 					<div class="col-12">
			 						<span class="spamfont " > Current date and time: ${time}</span>
								</div>
			 				</div>
			 				<!-- button -->
			 				<div class="row justify-content-start">
				 				<div class="col-md-6 col-sm-4 w-100">
				 					<a href="https://www.google.com/maps/place/${i.name}" class="btn btn-outline-primary rounded-0 font-weight-bold show-map active w-100 " role="button" aria-pressed="true">Show Map</a>
				 				</div>
				 				<div class="col-md-6 col-sm-4 w-100 ">
				 					<button type="button" class="btn btn-outline-primary flex-fill font-weight-bold rounded-0 w-100 show-map "onclick="clickdetail(this.value) " value="${i.alpha3Code}""><a href="detail.html" target="_blank">Detail</a></button>
				 				</div>
			 				</div>
			 			</div>
			 		</div>
			 	</div>
			</div>`;
		}
	card.innerHTML=str;
}
function clickdetail(alpha3code){
	window.localStorage.setItem('countrycode', alpha3code);
	console.log(alpha3code)
}

function dateTime(timezone){
	let offset1 = timezone.split("C");
	let offset2 = offset1[1].split(":");
	let offset3=parseInt(offset2[0]);
	let offset4 =parseInt(offset2[1]);
	let offset = offset3+(offset4/60);
	let x = new Date();
	let month = x.getMonth();
	let months =["Jan", "Feb", "Mar", "Apr", "May", "Jun",
    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
	let day = x.getDate();
	let year = x.getFullYear();
	let str1 = ""
	let str=""
	let str2= ""
	str1= day+" " +months[month] + " "+ year;
	let localTime = x.getTime();
	let localOffset = x.getTimezoneOffset() * 60000;
	let utc = localTime + localOffset;
	let nd = new Date(utc + (3600000*offset));
	let h = nd.getHours();
	let m = nd.getMinutes();
	let h1="";
	let m1 ="";
	if(h<10){
		h1=h1+"0"+h.toString();
	}
	if(m<10){
		m1=m1+"0"+m.toString();
	}
	if(h<10){
		if(m<10){
			str2 = h1+":"+m1;
		}
		else{
			str2 = h1+":"+m;
		}
	}
	else{
		if(m<10){
			str2 = h+":"+m1;
		}
		else{
			str2 = h+":"+m;
		}
	}
	str=str1+" and "+str2;
	console.log(str);
	
	return(str)
}

